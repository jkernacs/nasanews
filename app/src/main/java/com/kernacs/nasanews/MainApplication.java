package com.kernacs.nasanews;

import android.support.multidex.MultiDexApplication;

/**
 * The main application.
 */
public class MainApplication extends MultiDexApplication {
	public static MainApplication INSTANCE;

	@Override
	public void onCreate() {
		super.onCreate();
		INSTANCE = this;
	}

}
