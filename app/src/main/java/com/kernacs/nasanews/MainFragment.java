package com.kernacs.nasanews;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kernacs.nasanews.adapter.RssRecyclerViewAdapter;
import com.kernacs.nasanews.communication.NasaNewsAPI;
import com.kernacs.nasanews.communication.data.Feed;
import com.kernacs.nasanews.communication.data.FeedItem;
import com.kernacs.nasanews.databinding.FragmentMainBinding;
import com.kernacs.nasanews.di.component.NewsComponent;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.kernacs.nasanews.communication.CommunicationUtils.createRetrofitClient;

/**
 * The main fragment containing the refreshable list.
 */
public class MainFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

	private static final String TAG = MainFragment.class.getName();

	private static final String STATE_KEY_ADAPTER_ITEMS = "MainFragment:STATE_KEY_ADAPTER_ITEMS";

	@Inject
	protected GridLayoutManager gridLayoutManager;

	@Inject
	protected RssRecyclerViewAdapter adapter;

	@Inject
	protected RecyclerView.ItemDecoration itemDecoration;

	private FragmentMainBinding fragmentMainBinding;

	private NasaNewsAPI service;

	private Subscription subscription;

	private Observable<Feed> observable;

	private Observer observer;


	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		NewsComponent.NewsModuleInjector.INSTANCE.inject(this);

		service = createRetrofitClient(getResources());
		observable = service.getItems()
				.subscribeOn(Schedulers.newThread())
				.observeOn(AndroidSchedulers.mainThread());
		observer = new Observer<Feed>() {
			@Override
			public void onCompleted() {
				Log.d(TAG, "Download of RSS Items completed");
				subscription.unsubscribe();
			}

			@Override
			public void onError(final Throwable e) {
				Log.e(TAG, "Failed to download RSS Items", e);
				showRefreshView();
				stopRefreshing();
			}

			@Override
			public void onNext(final Feed feed) {
				Log.d(TAG, "onNext gets called");
				adapter.setItems(feed.getChannel().getFeedItems());
				setupViews();
				stopRefreshing();
			}
		};
	}

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
		fragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
		return fragmentMainBinding.getRoot();
	}


	@Override
	public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		fragmentMainBinding.swipeRefreshLayout.setOnRefreshListener(this);
		fragmentMainBinding.refreshView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view1) {
				MainFragment.this.loadData();
			}
		});
		fragmentMainBinding.recyclerView.setLayoutManager(gridLayoutManager);
		fragmentMainBinding.recyclerView.setHasFixedSize(true);
		fragmentMainBinding.recyclerView.addItemDecoration(itemDecoration);
		fragmentMainBinding.recyclerView.setAdapter(adapter);
		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_KEY_ADAPTER_ITEMS)) {
			adapter.setItems((ArrayList<FeedItem>) savedInstanceState.getSerializable(STATE_KEY_ADAPTER_ITEMS));
		} else {
			loadData();
		}
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(STATE_KEY_ADAPTER_ITEMS, new ArrayList<>(adapter.getItems()));
	}

	private void loadData() {
		subscription = observable.subscribe(observer);
	}

	private void stopRefreshing() {
		fragmentMainBinding.swipeRefreshLayout.setRefreshing(false);
	}

	@Override
	public void onRefresh() {
		loadData();
	}

	public void search(String query) {
		adapter.setFilter(query);
		setupViews();
	}

	private void setupViews() {
		if (adapter.getItemCount() == 0) {
			showEmptyView();
		} else {
			hideInfoViews();
		}
	}

	protected void hideInfoViews() {
		fragmentMainBinding.emptyView.setVisibility(View.GONE);
		fragmentMainBinding.refreshView.setVisibility(View.GONE);
	}

	protected void showEmptyView() {
		fragmentMainBinding.emptyView.setVisibility(View.VISIBLE);
		fragmentMainBinding.refreshView.setVisibility(View.GONE);
	}

	protected void showRefreshView() {
		fragmentMainBinding.emptyView.setVisibility(View.GONE);
		fragmentMainBinding.refreshView.setVisibility(View.VISIBLE);
	}

}
