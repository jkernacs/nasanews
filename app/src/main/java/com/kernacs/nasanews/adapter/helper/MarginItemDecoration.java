package com.kernacs.nasanews.adapter.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kernacs.nasanews.R;

/**
 * Item decoration for the RecyclerView of the main list.
 */
public class MarginItemDecoration extends RecyclerView.ItemDecoration {

	@Override
	public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
		final int horizontalMargin = parent.getContext().getResources().getDimensionPixelOffset(R.dimen.main_list_horizontal_margin);
		final int verticalMargin = parent.getContext().getResources().getDimensionPixelOffset(R.dimen.main_list_vertical_margin);
		final int pos = parent.getChildAdapterPosition(view);
		if (pos == 0) {
			outRect.top += verticalMargin;
		}
		outRect.bottom += verticalMargin;
		outRect.left += horizontalMargin;
		outRect.right += horizontalMargin;
	}
}
