package com.kernacs.nasanews.adapter.helper.impl;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.kernacs.nasanews.R;
import com.kernacs.nasanews.adapter.helper.RssAdapterHelper;
import com.kernacs.nasanews.di.component.NewsComponent;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

/**
 * Default implementation for the {@link RssAdapterHelper} interface;
 */
public class DefaultRssAdapterHelper implements RssAdapterHelper {

	@Inject
	protected Picasso picasso;

	@Inject
	protected Context context;

	/**
	 * Constructor to initialize the INSTANCE with injection.
	 */
	@Inject
	public DefaultRssAdapterHelper() {
		NewsComponent.NewsModuleInjector.INSTANCE.inject(this);
	}

	@Override
	public void openUrl(@NonNull final View view, @NonNull final String url) {
		String targetUrl = url;
		Intent intent = new Intent(Intent.ACTION_VIEW);
		if (!targetUrl.startsWith("http://") && !targetUrl.startsWith("https://")) {
			targetUrl = "http://" + targetUrl;
		}
		intent.setData(Uri.parse(targetUrl));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		view.setEnabled(true);
		context.startActivity(intent);
	}

	@Override
	public void loadImage(@NonNull final ImageView imageView, final String imageUrl) {
		if (TextUtils.isEmpty(imageUrl)) {
			imageView.setVisibility(View.GONE);
		} else {
			picasso.load(imageUrl)
					.placeholder(R.drawable.placeholder_image)
					.error(R.drawable.placeholder_image)
					.into(imageView);
		}
	}
}
