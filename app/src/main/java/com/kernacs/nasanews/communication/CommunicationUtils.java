package com.kernacs.nasanews.communication;

import android.content.res.Resources;

import com.kernacs.nasanews.R;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Utility class for Retrofit based communication setup.
 */
public final class CommunicationUtils {

	private CommunicationUtils() {
		throw new IllegalStateException("Private constructor for utility class");
	}

	/**
	 * Creates the {@link Retrofit} communicator.
	 *
	 * @param resources {@link Resources} INSTANCE to get the communication URL.
	 * @return the implementation of the API endpoint.
	 */
	public static NasaNewsAPI createRetrofitClient(final Resources resources) {
		final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		final OkHttpClient client = new OkHttpClient.Builder().
				addInterceptor(interceptor).build();

		final Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(resources.getString(R.string.nasa_news_rss_url))
				.addConverterFactory(SimpleXmlConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.client(client)
				.build();
		return retrofit.create(NasaNewsAPI.class);
	}
}