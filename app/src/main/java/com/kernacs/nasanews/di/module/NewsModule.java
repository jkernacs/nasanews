package com.kernacs.nasanews.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.kernacs.nasanews.MainApplication;
import com.kernacs.nasanews.R;
import com.kernacs.nasanews.adapter.RssRecyclerViewAdapter;
import com.kernacs.nasanews.adapter.helper.MarginItemDecoration;
import com.kernacs.nasanews.adapter.helper.RssAdapterHelper;
import com.kernacs.nasanews.adapter.helper.impl.DefaultRssAdapterHelper;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class NewsModule {
	public static final NewsModule INSTANCE = new NewsModule(MainApplication.INSTANCE);
	private final Context context;

	public NewsModule(final Context context) {
		this.context = context;
	}

	@Provides
	Context context() {
		return context;
	}

	@Provides
	GridLayoutManager gridLayoutManager() {
		return new GridLayoutManager(context, context.getResources().getInteger(R.integer.grid_column_count));
	}

	@Provides
	RecyclerView.ItemDecoration provideMarginItemDecoration() {
		return new MarginItemDecoration();
	}

	@Provides
	RssRecyclerViewAdapter provideRssAdapter() {
		return new RssRecyclerViewAdapter();
	}

	@Provides
	RssAdapterHelper provideRssAdapterHelper() {
		return new DefaultRssAdapterHelper();
	}

	@Provides
	@Singleton
	SharedPreferences provideSharedPrefs() {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	@Provides
	Picasso picasso() {
		OkHttpClient client = new OkHttpClient().newBuilder()
				.connectTimeout(30, TimeUnit.SECONDS)
				.readTimeout(30, TimeUnit.SECONDS)
				.build();

		return new Picasso.Builder(context)
				.downloader(new OkHttp3Downloader(client))
				.memoryCache(new LruCache(context))
				.loggingEnabled(true)
				.build();
	}
}
